<?php


/**
 * Class Controller
 * @property Request $request
 * @property Response $response
 * @property Session $session
 * @property DbManager $db_Manager
 */
abstract class Controller
{
    /**
     * @var string
     */
    protected $controller_name;

    /**
     * @var
     */
    protected $action_name;

    /**
     * @var Application
     */
    protected $application;

    /**
     * @var mixed
     */
    protected $request;

    /**
     * @var mixed
     */
    protected $response;

    /**
     * @var mixed
     */
    protected $session;

    /**
     * @var mixed
     */
    protected $db_manager;

    /**
     * @var array
     */
    protected $auth_actions = [];

    /**
     * Controller constructor.
     * @param Application $application
     */
    public function __construct($application)
    {
        $this->controller_name = strtolower(substr(get_class($this), 0, 10));

        $this->application = $application;
        $this->request     = $application->getRequest();
        $this->response    = $application->getResponse();
        $this->session     = $application->getSession();
        $this->db_manager  = $application->getDbManager();
    }

    /**
     * @param $action
     * @param array $params
     * @return mixed
     * @throws HttpNotFoundException
     * @throws UnauthorizedActionException
     */
    public function run($action, $params = [])
    {
        $this->action_name = $action;

        $action_method = $action . 'Action';
        if (!method_exists($this, $action_method)) {
            $this->forward404();
        }

        if ($this->needsAuthentication($action) && !$this->session->isAuthenticated()) {
            throw new UnauthorizedActionException();
        }

        return $this->$action_method($params);
    }

    /**
     * @param $action
     * @return bool
     */
    protected function needsAuthentication($action)
    {
        if ($this->auth_actions === true || (is_array($this->auth_actions) && in_array($action, $this->auth_actions, true))) {
            return true;
        }
        return false;
    }


    /**
     * @param array $variable
     * @param null $template
     * @param string $layout
     * @return false|string
     */
    public function render($variable = [], $template = null, $layout = 'layout')
    {
        $defaults = [
            'request'   => $this->request,
            'base_url'  => $this->request->getBaseUrl(),
            'session'   => $this->session,
        ];

        $view = new View($this->application->getViewDir(), $defaults);

        if ($template === null) {
            $template = $this->action_name;
        }

        $path = $this->controller_name . '/' . $template;

        return $view->render($path, $variable, $layout);
    }

    /**
     * @throws HttpNotFoundException
     */
    protected function forward404()
    {
        throw new HttpNotFoundException('Forwarded 404 page from '
            . $this->controller_name . '/' . $this->action_name
        );
    }

    /**
     * @param $url
     */
    protected function redirect($url)
    {
        if (!preg_match($url, '#https?://#')) {
            $protocol = $this->request->isSsl() ? 'https://' : 'http://';
            $host = $this->request->getHost();
            $base_url = $this->request->getBaseUrl();

            $url = $protocol . $host . $base_url . $url;
        }

        $this->response->setStatusCode(302, 'Found');
        $this->response->setHttpHeader('Location', $url);
    }

    /**
     * @param $from_name
     * @return string
     */
    public function generateCsrfToken($from_name)
    {
        $key = 'csrf_tokens/' . $from_name;
        $tokens = $this->session->get($key, []);
        if (count($tokens) >= 10) {
            array_shift($tokens);
        }

        $token = sha1($from_name . session_id() . microtime());
        $tokens[] = $token;

        $this->session->set($key, $tokens);

        return $token;
    }

    /**
     * @param $from_name
     * @param $token
     * @return bool
     */
    protected function checkCsrfToken($from_name, $token)
    {
        $key = 'csrf_tokens/' . $from_name;
        $tokens = $this->session->get($key, []);

        if (false !== ($pos = array_search($token, $tokens, true))) {
            unset($tokens[$pos]);
            $this->session->set($key, $tokens);

            return true;
        }
        return false;

    }
}