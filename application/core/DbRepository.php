<?php

abstract class DbRepository
{
    /**
     * @var
     */
    protected $con;

    /**
     * DbRepository constructor.
     * @param $con
     */
    public function __construct($con)
    {
        $this->setConnection($con);
    }

    /**
     * @param $con
     */
    public function setConnection($con)
    {
        $this->con = $con;
    }

    /**
     * @param $sql
     * @param array $params
     * @return mixed
     */
    public function execute($sql, $params = [])
    {
        $stmt = $this->con->prepare($sql);
        $stmt->execute($params);

        return $stmt;
    }

    /**
     * @param $sql
     * @param array $params
     * @return mixed
     */
    public function fetch($sql, $params = [])
    {
        return $this->execute($sql, $params)->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param $sql
     * @param array $params
     * @return mixed
     */
    public function fetchAll($sql, $params = [])
    {
        return $this->execute($sql, $params)->fetchAll(PDO::FETCH_ASSOC);
    }
}
