<?php


/**
 * Class View
 */
class View
{
    /**
     * @var
     */
    protected $base_dir;

    /**
     * @var
     */
    protected $defaults;

    /**
     * @var array
     */
    protected $layout_variables = [];

    /**
     * View constructor.
     * @param $base_dir
     * @param $defaults
     */
    public function __construct($base_dir, $defaults)
    {
        $this->base_dir = $base_dir;
        $this->defaults = $defaults;
    }

    /**
     * @param $name
     * @param $value
     */
    public function setLayoutVar($name, $value)
    {
        $this->layout_variables[$name] = $value;
    }

    /**
     * @param $_path
     * @param array $_variables
     * @param bool $_layout
     * @return false|string
     */
    public function render($_path, $_variables = [], $_layout = false)
    {
        $_file = $this->base_dir . '/' . $_path . '.php';

        extract(array_merge($this->defaults, $_variables));

        ob_start();
        ob_implicit_flush(0);

        require $_file;

        $content = ob_get_clean();

        if ($_layout) {
            $content = $this->render(
                $_layout,
                array_merge($this->layout_variables, [
                    '_content' => $content,
                ])
            );
        }

        return $content;
    }

    /**
     * @param $string
     * @return string
     */
    public function escape($string)
    {
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    }

}
